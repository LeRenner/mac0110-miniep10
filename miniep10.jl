function calculate_subsum(matrix, start_coords, size)
    sum = 0
    for i in 0:(size - 1)
        for j in 0:(size - 1)
            sum += matrix[start_coords[1] + i, start_coords[2] + j]
        end
    end
    return sum
end

function max_sum_submatrix(matrix)
    size = Int(sqrt(length(matrix)))
    max_subsum_composision = Array{Any}(nothing, 0)
    max_subsum_value = -1
    found_non_negative_solution = false

    for i in 1:size
        for j in 1:size
            max_dimension = min(size - j + 1, size - i + 1)
            for k in 1:max_dimension
                subsum = calculate_subsum(matrix, [i, j], k)
                if subsum > max_subsum_value
                    found_non_negative_solution = true
                    max_subsum_value = subsum
                    max_subsum_composision = Array{Any}(nothing, 0)
                    push!(max_subsum_composision, [i, j, k])
                elseif subsum == max_subsum_value && found_non_negative_solution
                    push!(max_subsum_composision, [i, j, k])
                end
            end
        end
    end

    if ~(found_non_negative_solution)
        return Array{Any}(nothing, 0)
    else
        number_of_solutions = length(max_subsum_composision)
        final_vector = Array{Any}(nothing, 0)
        for i in 1:number_of_solutions
            index_1 = max_subsum_composision[i][1]
            index_2 = max_subsum_composision[i][1] + max_subsum_composision[i][3] - 1
            index_3 = max_subsum_composision[i][2]
            index_4 = max_subsum_composision[i][2] + max_subsum_composision[i][3] - 1
            push!(final_vector, matrix[index_1:index_2, index_3:index_4])
        end
        return final_vector
    end
end

using Test
function test()
    result = max_sum_submatrix([-1 1 1 ; 1 -1 1 ; 1 1 -2])
    @test length(result) == 3
    for i in 1:length(result)
        @test (result[i] .== Any[[-1 1 1; 1 -1 1; 1 1 -2], [1 1; -1 1], [1 -1; 1 1]][i])[1]
    end

    result = max_sum_submatrix([-1 -2 ; -3 -4])
    @test result == Any[]

    result = max_sum_submatrix([0 -1 ; -2 -3])
    @test length(result) == 1
    @test (result[1] .== Any[[0]][1])[1]

    result = max_sum_submatrix([1 1 1 ; 1 1 1 ; 1 1 1])
    @test length(result) == 1
    @test (result[1] .== Any[[1 1 1 ; 1 1 1 ; 1 1 1]][1])[1]

    println("Final dos testes!")
end

test()
